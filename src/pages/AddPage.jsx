import React, {useState} from 'react';
import Header from "../components/Header";
import {Button, Container, Form, Row} from "react-bootstrap";
import {useDispatch} from "react-redux";

const AddPage = () => {
    const [title, setTitle] = useState('')
    const [desc, setDesc] = useState('')
    const [video, setVideo] = useState('')
    const [file, setFile] = useState(null)
    const dispatch = useDispatch()

    const selectFile = (e) => {
        setFile(e.target.files[0])
    }

    const addFilm = (e) => {
        e.preventDefault()
        const data = {
            id: Date.now(),
            title: title,
            desc: desc,
            video: video,
            img: file
        }
        console.log(data)
        dispatch({type: "CREATE_FILM", data})
    }

    return (
        <>
            <Header/>
            <Container>
                <Row>
                    <Form className='mt-5'>
                        <Form.Group className="mb-4" controlId="formBasicEmail">
                            <Form.Label>Title</Form.Label>
                            <Form.Control value={title} onChange={e => setTitle(e.target.value)} type="text" placeholder="Title" />
                        </Form.Group>

                        <Form.Group className="mb-4" controlId="formBasicPassword">
                            <Form.Label>Description</Form.Label>
                            <Form.Control value={desc} onChange={e => setDesc(e.target.value)} type="text" placeholder="Description" />
                        </Form.Group>

                        <Form.Group className="mb-4" controlId="formBasicEmail">
                            <Form.Label>Video</Form.Label>
                            <Form.Control value={video} onChange={e => setVideo(e.target.value)} type="text" placeholder="Video" />
                        </Form.Group>

                        <Form.Group controlId="formFile" className="mb-3">
                            <Form.Label>Upload image</Form.Label>
                            <Form.Control onChange={selectFile} type="file" />
                        </Form.Group>

                        <Button variant="primary" type="submit" onClick={addFilm}>Submit</Button>
                    </Form>
                </Row>
            </Container>
        </>
    );
};

export default AddPage;