import React from 'react';
import Header from '../components/Header';
import {Container} from 'react-bootstrap';
import {useSelector} from 'react-redux';
import FavoriteItem from '../components/FavoriteItem';

const FavoritePage = () => {
    const film = useSelector(state => state.favorite)
    console.log("film fav", film);
    return (
        <div>
            <Header />
            <Container>
                {film.map((id) =>
                    <FavoriteItem key={id} cardId={id} />
                )}
            </Container>
        </div>
    );
};

export default FavoritePage;