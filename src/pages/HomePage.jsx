import React from 'react';
import Header from "../components/Header";
import {Container} from "react-bootstrap";
import NavBar from "../components/NavBar";
import Cards from "../components/Cards";

const HomePage = () => {
    return (
        <>
            <Header/>
            <Container>
                <NavBar/>
                <Cards/>
            </Container>
        </>
    );
};

export default HomePage;