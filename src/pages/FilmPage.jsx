import React from 'react';
import Header from "../components/Header";
import {Card, Col, Container, Row} from "react-bootstrap";
import Iframe from 'react-iframe'
import {useParams} from "react-router-dom";
import {useSelector} from "react-redux";
const FilmPage = () => {
    const params = useParams()
    const id = params.id - 1
    const film = useSelector(state => state.films[id])

    return (
        <>
            <Header/>
            <Container>
                <Row>
                    <Col className={'mt-5'} md={'9'}>
                        <Iframe url={film.video}
                                width="640px"
                                height="320px"
                                id=""
                                className=""
                                display="block"
                                position="relative"/>
                    </Col>
                    <Col className={'mt-5'} md={'3'}>
                        <Card>
                            <Card.Body>
                                <Card.Title>{film.title}</Card.Title>
                                <Card.Text>
                                    {film.desc}
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    );
};

export default FilmPage;