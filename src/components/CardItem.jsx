import React from 'react';
import {Button, Card, Col} from "react-bootstrap";
import {useNavigate} from 'react-router-dom'
import {useDispatch} from 'react-redux';

const CardItem = ({prop}) => {
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const addFavorite = (id) => {
        dispatch({type: "ADD_FAVORITE", id})
    }
    return (
        <>
            <Col className='mt-3' md={'3'}>
                <Card>
                    <Card.Img
                        height={'430px'}
                        variant="top"
                        src={prop.img}
                        onClick={()=> navigate('/' + prop.id)}
                    />
                    <Card.Body>
                        <Card.Title>{prop.title}</Card.Title>
                        <Card.Text>
                            {prop.desc}
                        </Card.Text>
                        <Button onClick={() => {addFavorite(prop.id)}} variant="primary">Add favorite</Button>
                    </Card.Body>
                </Card>
            </Col>
        </>
    );
};

export default CardItem;