import React from 'react';
import {Nav, Row} from "react-bootstrap";

const NavBar = () => {
    return (
        <Row>
            <Nav className='mt-3'>
                <Nav.Item>
                    <Nav.Link  href="/home">Active</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="link-1">Link</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="link-2">Link</Nav.Link>
                </Nav.Item>
            </Nav>
        </Row>
    );
};

export default NavBar;