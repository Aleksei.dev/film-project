import React from 'react';
import {Col, Container, Nav, Row} from "react-bootstrap";
import {Link} from "react-router-dom";

const Header = () => {
    return (
        <header>
            <Container>
                <Row>
                    <Col md={2}>
                        <h1>Films</h1>
                    </Col>
                    <Col md={10}>
                        <nav className='header__nav'>
                            <Link className='me-5 white' to="/">Home</Link>
                            <Link className='me-5 white' to="/add">Add film</Link>
                            <Link className='white' to="/favorite">Favorite film</Link>
                        </nav>
                    </Col>
                </Row>
            </Container>
        </header>
    );
};

export default Header;