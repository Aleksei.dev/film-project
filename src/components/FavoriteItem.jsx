import React from 'react';
import {useSelector} from 'react-redux';
import {Card, Col, Row} from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';

const FavoriteItem = ({cardId}) => {
    console.log("cardId", cardId);
    const navigate = useNavigate()
    const film = useSelector(state => state.films.filter(film => film.id === cardId.id))[0]
    console.log('film render', film);
    return (
        <Row className={'mt-5'}>
            <Col md={2}></Col>
            <Col md={3}>
                <Card>
                    <Card.Img
                        width={'100%'}
                        height={'300px'}
                        variant="top"
                        src={film.img}
                        onClick={()=> navigate('/' + film.id)}
                    />
                </Card>
            </Col>
            <Col md={6}>
                <Card>
                    <Card.Body>
                        <Card.Title>{film.title}</Card.Title>
                        <Card.Text>
                            {film.desc}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    );
};

export default FavoriteItem;