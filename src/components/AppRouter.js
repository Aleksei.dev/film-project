import React from 'react';
import {Route, Routes} from 'react-router-dom'
import HomePage from "../pages/HomePage";
import AddPage from "../pages/AddPage";
import FilmPage from "../pages/FilmPage";
import FavoritePage from '../pages/FavoritePage';
const AppRouter = () => {
    return (
        <Routes>
            <Route path={'/'} element={<HomePage/>}/>
            <Route path={'/add'} element={<AddPage/>}/>
            <Route path={'/favorite'} element={<FavoritePage/>}/>
            <Route path={'/:id'} element={<FilmPage/>}/>
        </Routes>
    );
};

export default AppRouter;