import React from 'react';
import {Row} from "react-bootstrap";
import CardItem from "./CardItem";

import src1 from "../assets/1.jpg"
import src2 from "../assets/2.jpg"
import src3 from "../assets/3.jpg"
import {useSelector} from "react-redux";


const Cards = () => {
    const products = useSelector(state => state.films)
    return (
        <Row>
            {products.map(item =>
                <CardItem
                    key={item.id}
                    prop={item}
                />
            )}
        </Row>
    );
};

export default Cards;