import src1 from "../assets/1.jpg";
import src2 from "../assets/2.jpg";
import src3 from "../assets/3.jpg";

const initialState = {
    films: [
        {
            id: 1,
            title: 'Interstellar',
            desc: 'It stars Matthew McConaughey, Anne Hathaway, Jessica Chastain, Bill Irwin, Ellen Burstyn, Matt Damon, and Michael Caine. Set in a dystopian future where ...',
            img: src1,
            video: 'https://www.youtube.com/embed/zSWdZVtXT7E'
        },
        {
            id: 2,
            title: 'House of the dragon',
            desc: 'It stars Matthew McConaughey, Anne Hathaway, Jessica Chastain, Bill Irwin, Ellen Burstyn, Matt Damon, and Michael Caine. Set in a dystopian future where ...',
            img: src2,
            video: 'https://www.youtube.com/embed/DotnJ7tTA34'
        },
        {
            id: 3,
            title: 'Back to future',
            desc: 'It stars Matthew McConaughey, Anne Hathaway, Jessica Chastain, Bill Irwin, Ellen Burstyn, Matt Damon, and Michael Caine. Set in a dystopian future where ...',
            img: src3,
            video: 'https://www.youtube.com/embed/C-4Pr9kBtGc'
        },
    ],
    favorite: []
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "ADD_FAVORITE":
            console.log('new fav',{...state, favorite: [...state.favorite, {id: action.id}]});
            return {...state, favorite: [...state.favorite, {id: action.id}]}
        case "CREATE_FILM":
            return {...state, films: [...state.films, action.data]}
        default:
            return state
    }
}